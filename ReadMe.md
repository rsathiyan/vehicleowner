##How To Run

###First build docker image

`./gradlew jibDockerBuild --image=Garage/vehicleowner`

###Then run the docker

`docker run -p 8080:8080 --net="host" -t Garage/vehicleowner`