package com.auth.vehicleownerside.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="userlog")
public class UserX {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String password;

    @OneToMany(mappedBy = "vehiclenumber", cascade = CascadeType.ALL)
    @JsonIgnore

    private Set<Vehicle> vehicles;

    public UserX() {
    }

    public UserX(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserX(Long id, String loggedInUser, String encode) {
        this.id=id;
        this.username=loggedInUser;
        this.password=encode;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password){
        this.password=password;
    }

    public Set<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(Set<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    public void setUsername(String username) {
        this.username=username;
    }
}
