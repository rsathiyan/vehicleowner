package com.auth.vehicleownerside.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name="vehiclelog")
public class Vehicle {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String vehiclename;
    private String vehiclenumber;
    private String time;
//    private String username;

    @ManyToOne
    @JoinColumn
    @JsonIgnore

    private UserX username;

    public Vehicle() {
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Vehicle(String vehiclename, String vehiclenumber, UserX username, String time) {
        this.vehiclename = vehiclename;
        this.vehiclenumber = vehiclenumber;
        this.username=username;
        this.time=time;

    }

    public Vehicle(String vehiclename, String vehiclenumber, UserX username) {
        this.vehiclename = vehiclename;
        this.vehiclenumber = vehiclenumber;
        this.username = username;
    }

    public Long getId() {
        return id;
    }


    public UserX getUsername() {
        return username;
    }

    public void setUsername(UserX username) {
        this.username = username;
    }

    public String getVehiclename() {
        return vehiclename;
    }

    public void setVehiclename(String vehiclename) {
        this.vehiclename = vehiclename;
    }

    public String getVehiclenumber() {
        return vehiclenumber;
    }

    public void setVehiclenumber(String vehiclenumber) {
        this.vehiclenumber = vehiclenumber;
    }
}

