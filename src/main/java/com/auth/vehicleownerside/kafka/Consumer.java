package com.auth.vehicleownerside.kafka;

import com.auth.vehicleownerside.repository.VehicleRepository;
import com.auth.vehicleownerside.service.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class Consumer {

    @Autowired
    JwtUserDetailsService userDetailsService;

    @Autowired
    VehicleRepository vehicleRepository;

    @KafkaListener(topics = "bookingtime", groupId = "whatever")
    public void listen(String message) {
        System.out.println("Received Messasge : " + message);

        //lets assume username,vehicle_number,time

        String[] respArr = message.split(",");

        vehicleRepository.updatetime(respArr[2],respArr[1]);


    }
}
