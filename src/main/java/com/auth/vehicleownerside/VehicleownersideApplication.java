package com.auth.vehicleownerside;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class VehicleownersideApplication {

    public static void main(String[] args) {
        SpringApplication.run(VehicleownersideApplication.class, args);
    }

}