package com.auth.vehicleownerside.repository;

import com.auth.vehicleownerside.entity.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface VehicleRepository extends JpaRepository<Vehicle, Long> {
    List<Vehicle> findByUsername_Id(Long username_id);

    Vehicle findByVehiclenumber(String vehiclenumber);

    @Transactional
    @Modifying
    @Query("UPDATE Vehicle v SET v.time = :time WHERE v.vehiclenumber = :vehiclenumber")
    void updatetime(@Param("time") String time, @Param("vehiclenumber") String vehiclenumber);

}
