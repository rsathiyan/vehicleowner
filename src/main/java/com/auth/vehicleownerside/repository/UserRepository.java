package com.auth.vehicleownerside.repository;

import com.auth.vehicleownerside.entity.UserX;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserX, Long> {

    UserX findByUsername(String username);
}
