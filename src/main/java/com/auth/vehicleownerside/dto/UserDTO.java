package com.auth.vehicleownerside.dto;


public class UserDTO {

    private Long id;
    private String username;
    private String password;

    private UserDTO user;

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
