package com.auth.vehicleownerside.dto;


public class VehicleDTO {

    private Long id;
    private String vehiclename;
    private String vehiclenumber;
    private String username;

    private VehicleDTO vehicle;

    public Long getId() {
        return id;
    }

    public String getVehiclename() {
        return vehiclename;
    }

    public String getVehiclenumber() {
        return vehiclenumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
