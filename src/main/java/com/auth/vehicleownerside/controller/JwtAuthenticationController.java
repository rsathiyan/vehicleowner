package com.auth.vehicleownerside.controller;

import com.auth.vehicleownerside.config.JwtRequest;
import com.auth.vehicleownerside.config.JwtResponse;
import com.auth.vehicleownerside.config.JwtTokenUtil;
import com.auth.vehicleownerside.dto.BookingDto;
import com.auth.vehicleownerside.dto.VehicleDTO;
import com.auth.vehicleownerside.entity.UserX;
import com.auth.vehicleownerside.entity.Vehicle;
import com.auth.vehicleownerside.kafka.Producer;
import com.auth.vehicleownerside.repository.UserRepository;
import com.auth.vehicleownerside.service.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
//import com.javainuse.service.JwtUserDetailsService;


@RestController
@CrossOrigin
public class JwtAuthenticationController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
//    private UserDetailsService userDetailsService;
    private JwtUserDetailsService userDetailsService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    Producer kafkaProducer;


    @RequestMapping(value = "/authenticated", method = RequestMethod.POST)
    public ResponseEntity createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {


        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());

        authenticate(userDetails, authenticationRequest.getPassword());
        final String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new JwtResponse(token,"ok"));

    }


    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody JwtRequest request) throws Exception {


        System.out.println(Objects.isNull(userDetailsService.findByUsername(request.getUsername())));
        if (Objects.isNull(userDetailsService.findByUsername(request.getUsername()))) {
            userDetailsService.saveUser(new UserX(request.getUsername(), new BCryptPasswordEncoder().encode(request.getPassword())));
            UserDetails userDetails = userDetailsService.loadUserByUsername(request.getUsername());
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userDetails, request.getPassword()));
            final String token = jwtTokenUtil.generateToken(userDetails);
//logout
            return ResponseEntity.ok(new JwtResponse(token,"ok"));
        }
        else{
            System.out.println("USER ALREADY EXIST");
//            return (ResponseEntity) ResponseEntity.badRequest();
            return ResponseEntity.ok(new JwtResponse("","USER ALREADY EXIST"));

        }

    }


    @GetMapping("/update")
    public ResponseEntity<JwtResponse> updateUser(@RequestParam String username, @RequestParam String password) {

            UserX userX = userDetailsService.findByUsername(findLoggedInUser());
            userX.setUsername(username);
            userX.setPassword( new BCryptPasswordEncoder().encode(password));
            userDetailsService.saveUser(userX);
            System.out.println("UserUPDATE"+findLoggedInUser());
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userDetails, password));
            final String token = jwtTokenUtil.generateToken(userDetails);
        System.out.println("UserUPDATE2www"+findLoggedInUser());
//logout
            return ResponseEntity.ok(new JwtResponse(token,"ok"));

    }


    private void authenticate(UserDetails userDetails, String password) throws Exception {
        try {

            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userDetails, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    @RequestMapping(value = "/addVehicle", method = RequestMethod.POST)
    public void addVehicle(@RequestBody VehicleDTO req){
        System.out.println(req.getVehiclenumber());
        if (Objects.isNull(userDetailsService.findByVehiclenumber(req.getVehiclenumber()))) {

            if (findLoggedInUser() == "anonymousUser") {
                System.out.println("INVALID USER");
            } else {
                userDetailsService.saveVehicle(new Vehicle(req.getVehiclename(), req.getVehiclenumber(),userDetailsService.findByUsername(findLoggedInUser())));
            }
        }

    }


//    @GetMapping("viewVehicle")
//    public  viewVehicle(@RequestParam String username){
//
//        if(findLoggedInUser()==username){
//            userDetailsService.findByUsername(username);
//
//        }
//        else {
//
//        }
//
//
//
//
//    }



//    @GetMapping("/updateVehicle")
//    public String updateVehicle(@RequestParam String vehiclename, @RequestParam String vehiclenumber){
//
//        System.out.println(findLoggedInUser());
//            if (findLoggedInUser() == "anonymousUser") {
//                System.out.println("INVALID USER");
//                return "Err";
//            } else {
//                if(Objects.isNull(userDetailsService.findByVehiclenumber(vehiclenumber))){
//                    userDetailsService.saveVehicle(new Vehicle(vehiclename, vehiclenumber,userDetailsService.findByUsername(findLoggedInUser())));
//                }
//                else {
//                    Vehicle vehicle = userDetailsService.findByVehiclenumber(vehiclenumber);
//                    vehicle.setVehiclename(vehiclename);
//                    userDetailsService.saveVehicle(vehicle);
//                }
//                return "Succ";
//            }
//
//    }



    @RequestMapping(value = "/updateVehicle", method = RequestMethod.POST)
    public String updateVehicle(@RequestBody VehicleDTO req){

        System.out.println(findLoggedInUser());
        if (findLoggedInUser() == "anonymousUser") {
            System.out.println("INVALID USER");
            return "Err";
        } else {
            if(Objects.isNull(userDetailsService.findByVehiclenumber(req.getVehiclenumber()))){
                userDetailsService.saveVehicle(new Vehicle(req.getVehiclename(), req.getVehiclenumber(),userDetailsService.findByUsername(findLoggedInUser())));
            }
            else {
                Vehicle vehicle = userDetailsService.findByVehiclenumber(req.getVehiclenumber());
                vehicle.setVehiclename(req.getVehiclename());
                userDetailsService.saveVehicle(vehicle);
            }
            return "Succ";
        }

    }

    @RequestMapping(value = "/fetchVehicle", method = RequestMethod.POST)
    public ResponseEntity<List<Vehicle>> fetchVehicle(){
        List<Vehicle> vehicles = null;

        if(userDetailsService.findAllVehicle(findLoggedInUser()).isEmpty()){
            return ResponseEntity.ok(vehicles);
        }
        else{
            return ResponseEntity.ok(userDetailsService.findAllVehicle(findLoggedInUser()));

        }

    }

    @RequestMapping(value = "/deleteVehicle", method = RequestMethod.POST)
    public String deletevehicle(@RequestBody VehicleDTO req){

        System.out.println(req.getVehiclenumber());

        Vehicle vehicle = userDetailsService.findByVehiclenumber(req.getVehiclenumber());

        userDetailsService.delete(vehicle);
       return  "succ";
    }





    @PostMapping("/logout")
    public void logout(){

    }

    @GetMapping("/check")
    private String findLoggedInUser(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username="INVALID";
        if (principal instanceof UserDetails) {
             username = ((UserDetails)principal).getUsername();
        } else {
             username = principal.toString();
        }
//        UserDetails userDetails= SecurityContextHolder.getContext().getAuthentication();
//            return userDetails.getUsername();
        return username;
    }



    @GetMapping("/testkafka")
    private void sendKafka(){
        System.out.println("working kafka");

        String topic = findLoggedInUser();

        kafkaProducer.sendMessage("sathiyan", "This is a test");
    }

    @RequestMapping(value = "/bookvehicle", method = RequestMethod.POST)
    private void bookvehicle(@RequestBody VehicleDTO req){
        System.out.println("working kafka");

        System.out.println(req.getVehiclenumber());

        BookingDto bookingDto = new BookingDto();
        bookingDto.setUsername(findLoggedInUser());
        bookingDto.setVehiclenumber(req.getVehiclenumber());

        String topic = findLoggedInUser();

        kafkaProducer.sendMessage("booking_vehicle", bookingDto);
    }


}
