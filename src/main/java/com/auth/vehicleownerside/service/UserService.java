package com.auth.vehicleownerside.service;

import com.auth.vehicleownerside.entity.UserX;

public interface UserService {
    UserX findByUsername(String username);
}
