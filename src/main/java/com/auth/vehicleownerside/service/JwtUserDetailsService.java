package com.auth.vehicleownerside.service;

import com.auth.vehicleownerside.entity.UserX;
import com.auth.vehicleownerside.entity.Vehicle;
import com.auth.vehicleownerside.repository.UserRepository;
import com.auth.vehicleownerside.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VehicleRepository vehicleRepository;

//    @Override
    @Transactional(readOnly= true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserX userX = userRepository.findByUsername(username);
        if(userX ==null){
            throw new UsernameNotFoundException(username);
        }
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
       // grantedAuthorities.add(new SimpleGrantedAuthority("user"));
        return new org.springframework.security.core.userdetails.User(userX.getUsername(), userX.getPassword(),grantedAuthorities) ;
    }


    public void saveUser(UserX userX) {
        userRepository.save(userX);
    }

    public void saveVehicle(Vehicle vehicle) {
        vehicleRepository.save(vehicle);
    }

    public UserX findByUsername(String username) {
        return userRepository.findByUsername(username);
    }


    public Vehicle findByVehiclenumber(String vehiclenumber) {
        return vehicleRepository.findByVehiclenumber(vehiclenumber);

    }

    public List<Vehicle> findAllVehicle(String loggedInUser) {
        Long id = userRepository.findByUsername(loggedInUser).getId();
        return vehicleRepository.findByUsername_Id(id);
    }

    public void delete(Vehicle vehicle) {
        vehicleRepository.delete(vehicle);
    }
}