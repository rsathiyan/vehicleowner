package com.auth.vehicleownerside.service;

import com.auth.vehicleownerside.entity.UserX;
import com.auth.vehicleownerside.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserX findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

}
